# README #

Welcome to the README of Aliens VS French Coast's DCA aka tirocanon

### What is this repository for? ###

Team : Hatim-Pierre Fazileabasse <hatim.pierre.fazileabasse@gmail.com> 

### How do I get set up? ###
To use the game or make some tests, a few scripts are just what you need.
flash.sh run.sh or build.sh will do almost anything for you, feel free to use them!

### How to play? ###

Flash the board with the game, touch the screen to start. If you want to shoot, aim somewhere and press the screen. The further from the cannon, the more powerful the shot! 
Clouds don't award points but it's always fun shooting at them and seeing a trail form. You don't have infinite ammunition though, use them wisely... 

Aliens are coming for us... protect our lands!!