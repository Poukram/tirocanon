package graphics with SPARK_Mode => Off is

   type graphic is array (Natural range 0..13, Natural range 0..13) of Boolean;


   Alien : constant graphic :=
     ((False, False, False, False, False, False, False, False, False, False, False, False, False, False),
      (False, False, True , False, False, False, False, False, False, False, True , False, False, False),
      (False, False, True , False, False, False, False, False, False, False, True , False, False, False),
      (False, False, False, True , False, False, False, False, False, True , False, False, False, False),
      (False, False, False, True , False, True , True , True , False, True , False, False, False, False),
      (False, False, False, False, True , True , True , True , True , False, False, False, False, False),
      (True , True , False, False, True , False, True , False, True , False, False, True , True , False),
      (False, False, True , True , True , True , True , True , True , True , True , False, False, False),
      (False, False, False, False, True , True , True , True , True , False, False, False, False, False),
      (False, False, True , True , True , True , True , True , True , True , True , False, False, False),
      (False, True , False, False, True , False, False, False, True , False, False, True , False, False),
      (True , False, False, True , False, False, False, False, False, True , False, False, True , False),
      (False, False, False, True , False, False, False, False, False, True , False, False, False, False),
      (False, False, False, False, True , False, False, False, True , False, False, False, False, False));
end graphics;
