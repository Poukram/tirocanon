with draw_game_assets; use draw_game_assets;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Unchecked_Conversion;
with Text; use Text;

package body Game_State with SPARK_Mode => Off is

   function My_Time is
     new Ada.Unchecked_Conversion (Time, Unsigned_64);

   function My_Time is
     new Ada.Unchecked_Conversion (Duration, Unsigned_64);
   pragma Unreferenced (My_Time);

   procedure Initialize_Game(S : out State) is
   begin
      -- Data initialization
      S.Canon_Angle := Pi/4.0;
      S.Canon_end_barell := Rotate_About_Of((18.0,293.0), (20.0, 308.0),
                                            S.Canon_Angle);
      S.Shell_being_fired := False;
      S.Shells_Left := 10;
      S.Score := 0;
      S.Game_Over := False;
      S.Sh.Shell_fpos := (0.0, 0.0);
      S.Sh.Time_Fired := 0;
      S.Sh.Speed := 0.0;
      -- Drawing Phase
      Draw_Env;
      Draw_Canon(S.Canon_Angle);
      -- Touch to start ... Begin
      HomeScr_Txt;
      loop
         delay until Clock + Milliseconds (5);
         exit when Get_Touch_State.Touch_Detected;
      end loop;
      delay until Clock + Milliseconds (250);
      -- Touch to start ... Over
      Draw_Env;
      Text_Update;
      Enemy.Initialize(S.En, 1);
      Draw_Canon(S.Canon_Angle);
      Info_Text(S);
   end Initialize_Game;


   procedure Fire_Shell(S : in out State; P : Point) is
   begin
      Update_cannon_angle(S, P);
      if S.Shell_being_fired = False then
         S.Shell_being_fired := True;
         Shell_init_fpos(S);
         Shell_speed_set(S, P);
         S.Sh.Shell_fpos := S.Sh.Init_fpos;
         S.Sh.Time_Fired := My_Time(Clock);
         S.Sh.Angle_Fired := S.Canon_Angle;
         if S.Shells_Left = 1 then
            S.Game_Over := True;
         end if;
         S.Shells_Left := S.Shells_Left - 1;
         Clear_Infos;
         Info_Text(S);
      end if;
   end Fire_Shell;

   procedure Game_Over(S : in State) is
   begin
      Draw_Env;
      Draw_Canon(Pi/4.0);
      Info_Text(S);
      Print_Line("GAME OVER", (140, 70), 3);
      Print_Line("RESET BOARD FOR MORE", (105, 40), 2);
      loop
         delay until Clock + Seconds(1);
      end loop;
   end Game_Over;

   procedure Update(S : in out State) is
   begin
      -- Data update ... Begin
      if S.Shell_being_fired then
         Shell_update_pos(S);
         if Enemy.Touched(S.En, Shell_get_pos(S.Sh.Shell_fpos)) then
            Inc_Score(S);
            S.Shell_being_fired := False;
            Clear_Shell(Shell_get_pos(S.Sh.Prev_fpos));
            Clear_Infos;
            Info_Text(S);
            if S.Game_Over then
               Game_Over(S);
            else
               Enemy.Reset(S.En);
            end if;
         end if;
      else
         if S.Game_Over then
            Game_Over(S);
         end if;
      end if;
      -- Data update ... Over
      -- Graphics update ... Begin
      if S.Shell_being_fired then
         Draw_Shell(Shell_get_pos(S.Sh.Prev_fpos),
                    Shell_get_pos(S.Sh.Shell_fpos));
      end if;
      -- Graphics update ... Over
   end Update;

   function Shell_get_pos(FP : in FPoint) return Point is
      Res : Point;
   begin
      Res.X := Integer (FP.X / Width_Phy * Width_Pix);
      Res.Y := Integer (FP.Y / Height_Phy * Height_Pix);
      return Res;
   end Shell_get_pos;

   procedure Shell_speed_set(S : in out State; P : in Point) is
      Speed : Float;
      X : Float;
      Y : Float;
   begin
      X := S.Canon_end_barell.X - Float (P.X);
      Y := S.Canon_end_barell.Y - Float (P.Y);
      Speed := Sqrt(Pow(2, X) + Pow(2, Y));
      if Speed > 240.0 then
         Speed := 240.0;
      end if;
      Speed := Speed / 240.0 * 40.0; -- m.s^-1
      S.Sh.Speed := Speed;
   end Shell_speed_set;

   function Shell_check_pos(S : in State) return Boolean is
      X : Integer;
      Y : Integer;
   begin
      X := Integer (S.Sh.Shell_fpos.X / Width_Phy * Width_Pix);
      Y := Integer (S.Sh.Shell_fpos.Y / Height_Phy * Height_Pix);
      if X < 4 or Y < 4 or X > 236 or Y > 316 then
         return False;
      end if;
      return True;
   end Shell_check_pos;

   procedure Shell_init_fpos(S : in out State) is
   begin
      S.Sh.Init_fpos.X := S.Canon_end_barell.X / Width_Pix * Width_Phy;
      S.Sh.Init_fpos.Y := S.Canon_end_barell.Y / Height_Pix * Height_Phy;
      S.Sh.Prev_fpos := S.Sh.Init_fpos;
      S.Sh.Shell_fpos := S.Sh.Init_fpos;
   end Shell_init_fpos;

   procedure Update_cannon_angle(S: in out State; P : Point) is
      Vect : FPoint;
      Tmp : Float;
   begin
      Vect.X := Float (P.X) - S.Canon_end_barell.X;
      Vect.Y := Float (P.Y) - S.Canon_end_barell.Y;
      Normalize_Vect(Vect);
      Tmp := Acos(-Vect.Y);
      if Vect.X < 0.0 then
         Tmp := -1.0 * Tmp;
      end if;
      S.Canon_Angle := Tmp;
      S.Canon_end_barell := Rotate_About_Of((18.0,293.0), (20.0, 308.0), Tmp);
      -- Data and graphics mixed to avoid useless load on CPU
      Draw_Canon(S.Canon_Angle);
   end Update_cannon_angle;

   -- Units are m, s, m.s^-1, m.s^-2
   procedure Shell_update_pos(S : in out State) is
      v0 : Float;
      y0 : Float;
      x0 : Float;
      t : Float;
      g : constant := 9.81;
   begin
      v0 := S.Sh.Speed;
      y0 := S.Sh.Init_fpos.Y;
      x0 := S.Sh.Init_fpos.X;
      S.Sh.Prev_fpos := S.Sh.Shell_fpos;
      t := Float ((My_Time(Clock) - S.Sh.Time_Fired)) * Float(Time_Unit);
      S.Sh.Shell_fpos.Y := y0 - v0*t*Cos(S.Sh.Angle_Fired);
      S.Sh.Shell_fpos.X := x0 - 0.5*g*Pow(2,t) + v0*t*Sin(S.Sh.Angle_Fired);

      if Shell_check_pos(S) = False then
         S.Shell_being_fired := False;
         Clear_Shell(Shell_get_pos(S.Sh.Prev_fpos));
      end if;
   end Shell_update_pos;

   procedure Reset_Score(S : in out State) is
   begin
      S.Score := 0;
   end Reset_Score;

   procedure Inc_Score(S : in out State) is
   begin
      S.Score := S.Score + 10;
   end Inc_Score;

   procedure Enemy_Reset(S : in out State) is
   begin
      Enemy.Initialize(S.En, 1);
   end Enemy_Reset;

   procedure Text_Update is
   begin
      Print_Line("CLOUD", (100, 70), 2);
      Print_Line("CLOUD", (150, 30), 2);
      Print_Line("CLOUD", (200, 110), 1);
      Print_Line("CLOUD", (130, 225), 1);
      Print_Line("CLOUD", (139, 215), 1);
   end Text_Update;

   procedure HomeScr_Txt is
   begin
      Print_Line("ALIENS VS FRENCH COAST DCA", (135, 25), 2);
      Print_Line("TOUCH TO SAVE OUR LANDS", (105, 40), 2);
   end HomeScr_Txt;

   procedure Info_Text(S : in State) is
      res : String(1..1) := "D";
      amm : String(1..10) := "          ";
   begin
      -- Score infos
      Print_Line("SCORE", (225,0), 2);
      if S.Score < 20 then
         res := "D";
      elsif S.Score < 40 then
         res := "C";
           elsif S.Score <= 100 then
         res := "B";
           elsif S.Score > 100 then
         res := "A";
      end if;
      Print_Line(res, (225,55), 2);
      -- Ammo infos
      for I in 1..S.Shells_Left loop
         amm(amm'First+I-1) := 'I';
      end loop;
      Print_Line("SHOTS " & amm, (225, 140), 2);
   end Info_Text;
end Game_State;
