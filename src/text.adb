with font; use font;

package body Text with SPARK_Mode => Off is
   procedure Print_Number(Number : Character; Location : Point; Width : Positive) is
     To_Print : constant Symbole := NumSymboles(Number);
      Pt_Tmp : Point;
   begin
      for I in 1 .. 5 loop
         for J in 1 .. 5 loop
            if To_Print(J, I) then
               Pt_Tmp := ((5 - J) * Width, I * Width);
               for W1 in 1 .. Width loop
                  for W2 in 1 .. Width loop
                     Set_Pixel(Location + Pt_Tmp + (W2, W1), White);
                  end loop;
               end loop;
            end if;
         end loop;
      end loop;
   end Print_Number;

   procedure Print_Symbole(Letter : Character; Location : Point; Width : Positive) is
      To_Print : Symbole;
      Pt_Tmp : Point;
   begin
      if Letter = ' ' then
         return;
      end if;
      To_Print := Symboles(Letter);
      for I in 1 .. 5 loop
         for J in 1 .. 5 loop
            if To_Print(J, I) then
               Pt_Tmp := ((5 - J) * Width, I * Width);
               for W1 in 1 .. Width loop
                  for W2 in 1 .. Width loop
                     Set_Pixel(Location + Pt_Tmp + (W2, W1), White);
                  end loop;
               end loop;
            end if;
         end loop;
      end loop;
   end Print_Symbole;

   procedure Print_Line(Line : String; Location : Point; Width : Positive) is
   begin
      for Idx in Line'Range loop
         Print_Symbole(Line(Idx), Location + (0, 5 * Width * (Idx - Line'First)), Width);
      end loop;
   end Print_Line;


   procedure Print_NumLine(Line : String; Location : Point; Width : Positive) is
   begin
      for Idx in Line'Range loop
         Print_Number(Line(Idx), Location + (0, 5 * Width * (Idx - Line'First)), Width);
      end loop;
   end Print_NumLine;

end Text;
