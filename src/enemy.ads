with screen_interface; use screen_interface;

package Enemy with SPARK_Mode => Off is

   Type Enemy_Type is private;

   procedure Initialize(E : out Enemy_Type; Kind: in Positive);
   procedure Reset(E : in out Enemy_Type);
   function Touched(E : in Enemy_Type; Shell : in Point) return Boolean;

private

   procedure Pos_gen(E : in out Enemy_Type);
   procedure Hit(E : in out Enemy_Type);
   function IsDead(E : in Enemy_Type) return Boolean;
   type Enemy_Type is record
      Kind : Positive;
      Pos : Point;
      Prev : Point;
      Health : Natural;
   end record;
end Enemy;
