package Maths_Tools with SPARK_Mode => Off is

   type FPoint is record
      X : Float;
      Y : Float;
   end record;

   function "+" (P1, P2 : in FPoint) return FPoint
   is (P1.X + P2.X, P1.Y + P2.Y);
   function "-" (P1, P2 : in FPoint) return FPoint
   is (P1.X - P2.X, P1.Y - P2.Y);
   function "/" (P : in FPoint; D : in Float) return FPoint
   is (P.X / D, P.Y / D);

   Pi : constant :=
     3.14159_26535_89793_23846_26433_83279_50288_41971_69399_37511;
   Ln_2 : constant :=
     0.69314_71805_59945_30941_72321_21458;
   E : constant :=
     2.71828_18284_59045_23536_02874_71352_66249_77572_47093_69995_95749;
   function Ln(ToLn : in Float) return Float
     with Pre => ToLn > 0.0;
   function Sqrt(ToSqrt : in Float) return Float
     with
       Pre => ToSqrt >= 0.0,
       Post => Sqrt'Result >= 0.0;
   function Pow(Power : in Natural; ToPow : in Float) return Float;
   procedure Normalize_Vect(Vect : in out FPoint);
   function Fact(ToFact : in Natural) return Float;
   function Cos(Angle : in Float) return Float
     with
       Post => Cos'Result >= -1.0 and Cos'Result <= 1.0;
   function Acos(V: in Float) return Float
     with
       Pre => V > -1.0 and V < 1.0,
       Post => Acos'Result >= 0.0 and Acos'Result <= Pi;
   function Sin(Angle : in Float) return Float
     with
       Post => Sin'Result >= -1.0 and Sin'Result <= 1.0;

   function Rotate_About_Of(ToRotate : in FPoint; CenterOfR : in FPoint;
                            Angle : in Float) return FPoint
     with
       Pre => ToRotate.X >= 0.0 and ToRotate.X <= 240.0 and
     ToRotate.Y >= 0.0 and ToRotate.Y <= 320.0 and
     CenterOfR.X >= 0.0 and CenterOfR.Y <= 240.0 and
     CenterOfR.Y >= 0.0 and CenterOfR.Y <= 320.0,
     Post => Rotate_About_Of'Result.X >= 0.0 and
     Rotate_About_Of'Result.X <= 240.0 and
     Rotate_About_Of'Result.Y >= 0.0 and
     Rotate_About_Of'Result.Y <= 320.0;
   function LFSR return Integer;
end Maths_Tools;
