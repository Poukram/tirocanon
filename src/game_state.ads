with Maths_Tools; use Maths_Tools;
with Enemy; use Enemy;
with InterfaceS; use Interfaces;
with screen_interface; use screen_interface;

package Game_State with SPARK_Mode => Off is
   Type State is private;

   procedure Initialize_Game(S : out State);
   procedure Fire_Shell(S : in out State; P : Point);
   procedure Update(S : in out State);

private
   Height_Phy : constant := 100.0;
   Width_Phy : constant := 75.0;
   Height_Pix : constant := 320.0;
   Width_Pix : constant := 240.0;
   procedure Update_cannon_angle(S : in out State; P : Point);
   function Shell_get_pos(FP : in FPoint) return Point
     with
       Pre => FP.X >= 0.0 and FP.X <= Width_Phy and
     FP.Y >= 0.0 and FP.Y <= Height_Phy;

   function Shell_check_pos(S : in State) return Boolean
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;
   procedure Shell_init_fpos(S : in out State)
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;
   procedure Shell_speed_set(S : in out State; P : in Point)
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;
   procedure Shell_update_pos(S : in out State)
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;
   procedure Reset_Score(S : in out State)
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;
   procedure Inc_Score(S : in out  State)
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;
   procedure Enemy_Reset(S : in out State)
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;
   procedure Text_Update;
   procedure HomeScr_Txt;
   procedure Game_Over(S : in State)
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;
   procedure Info_Text(S : in State)
     with
       Pre => S.Canon_end_barell.X >= 0.0 and
       S.Canon_end_barell.X <= 240.0 and
       S.Canon_end_barell.Y >= 0.0 and
       S.Canon_end_barell.Y <= 320.0;

   Type Shell_T is record
      Shell_fpos : FPoint;
      Prev_fpos : FPoint;
      Init_fpos : Fpoint;
      Time_Fired : Unsigned_64;
      Angle_Fired : Float;
      Speed : Float;
   end record
     with Dynamic_Predicate => Shell_T.Speed >= 0.0;

   Type State is record
      Canon_Angle : Float;
      Canon_end_barell : FPoint;
      Shell_being_fired : Boolean;
      Shells_Left : Natural;
      Score : Natural;
      Game_Over : Boolean;
      Sh : Shell_T;
      En : Enemy_Type;
   end record;
end Game_State;
