with Maths_Tools; use Maths_Tools;
with draw_game_assets; use draw_game_assets;

package body Enemy with SPARK_Mode => Off is

  procedure Hit(E : in out Enemy_Type) is
      Damage : constant := 7;
   begin
      if E.Health < Damage then
         E.Health := 0;
      else
         E.Health := E.Health - Damage;
      end if;
   end Hit;

   function IsDead(E : in Enemy_Type) return Boolean is
   begin
      if E.Health = 0 then
         return True;
      end if;
      return False;
   end IsDead;

   procedure Initialize(E : out Enemy_Type; Kind: Positive) is
   begin
      E.Kind := Kind;

      if Kind = 1 then
         E.Health := 6;
      else
         E.Health := 12;
      end if;
      Pos_gen(E);
      E.Prev := E.Pos;
      Draw_Enemy(E.Prev, E.Pos);
   end Initialize;

   procedure Pos_gen(E : in out Enemy_Type) is
   begin
      -- Begin Hande no spawn zone
      E.Pos.X := (LFSR mod 170) + 40;

      if E.Pos.X <= 65 then
         E.Pos.Y := (LFSR mod 190);
      else
         E.Pos.Y := (LFSR mod 290);
      end if;
      -- End Handle no spawn zone
   end Pos_gen;


   procedure Reset(E : in out Enemy_Type) is
   begin
      E.Prev := E.Pos;
      Pos_gen(E);
      Draw_Enemy(E.Prev, E.Pos);
   end Reset;

   function Touched(E : in Enemy_Type; Shell : Point) return Boolean is
      Tmp : Point;
      X : Float;
      Y : Float;
      Dist : Float;
   begin
      Tmp.X := E.Pos.X + 7;
      Tmp.Y := E.Pos.Y + 7;
      X := Float (Tmp.X - Shell.X);
      Y := Float (Tmp.Y - Shell.Y);
      Dist := Pow(2, X) + Pow(2, Y);
      if Dist < 72.0 then
         return True;
      end if;
      return False;
   end Touched;

end Enemy;
