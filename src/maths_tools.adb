with Interfaces; use Interfaces;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Unchecked_Conversion;

package body Maths_Tools is

   -- Custom implem based on integer only method
   function Ln(ToLn : in Float) return Float is
      x : Float;
      tmp : Float;
      p : Natural := 0;
      Res : Float := 0.0;
      Max : constant := 1000;
   begin
      tmp := ToLn;
      loop
         p := p + 1;
         tmp := tmp / E;
         exit when tmp <= 2.0;
      end loop;
      x := 1.0 - tmp;
      for I in 1..Max loop
         Res := Res - (Pow(I, x)/Float (I));
      end loop;
      Res := Float (p) + Res;
      return Res;
   end Ln;

   function Pow(Power : in Natural; ToPow : in Float) return Float is
      Res : Float := 1.0;
   begin
      for I in 1..Power loop
         Res := Res* ToPow;
      end loop;
      return Res;
   end Pow;

   function Sqrt(ToSqrt : Float) return Float is
      Max : constant := 30;
      Ln_a : Float;
      Res : Float := 1.0;
   begin
      if ToSqrt = 0.0 then
         return 0.0;
      end if;
      Ln_a := Ln(ToSqrt);
      for I in 1..Max loop
         Res := Res + ((Pow(I, Ln_a)/Fact(I))*Pow(I, 0.5));
      end loop;
      return Res;
   end Sqrt;

   procedure Normalize_Vect(Vect : in out FPoint) is
      Dist : Float;
   begin
      Dist := Sqrt(Pow(2, Vect.X) + Pow(2, Vect.Y));
      Vect := Vect / Dist;
   end Normalize_Vect;

   function Fact(ToFact : Natural) return Float is
      Res : Float := 1.0;
   begin
      for I in 1..ToFact loop
         Res := Res * Float (I);
      end loop;
      return Res;
   end Fact;


   function Cos(Angle : Float) return Float is
      Res : Float;
   begin
      Res := 1.0;
      Res := Res - (Pow(2, Angle)/Fact(2));
      Res := Res + (Pow(4, Angle)/Fact(4));
      Res := Res - (Pow(6, Angle)/Fact(6));
      Res := Res + (Pow(8, Angle)/Fact(8));
      Res := Res - (Pow(10, Angle)/Fact(10));
      Res := Res + (Pow(12, Angle)/Fact(12));
      return Res;
   end Cos;

   function Acos(V: Float) return Float is
      Res : Float := 0.0;
      Tmp : Float := 0.0;
      Max : constant := 12;
   begin
      for n in 0 .. Max loop
         Tmp := (Fact(2*n)/Pow(2, Fact(n) * Pow(n, 2.0))) * (Pow(2*n+1, V)/Float (2*n+1));
         Res := Res + Tmp;
      end loop;
      Res := Pi/2.0 - Res;
      return Res;
   end Acos;

   function Sin(Angle : Float) return Float is
      Res : Float;
   begin
      Res := Angle;
      Res := Res - (Pow(3, Angle)/Fact(3));
      Res := Res + (Pow(5, Angle)/Fact(5));
      Res := Res - (Pow(7, Angle)/Fact(7));
      Res := Res + (Pow(9, Angle)/Fact(9));
      Res := Res - (Pow(11, Angle)/Fact(11));
      Res := Res + (Pow(13, Angle)/Fact(13));
      return Res;
   end Sin;


   function Rotate_About_Of(ToRotate : in FPoint; CenterOfR : in FPoint;
                            Angle : in Float) return FPoint is
      Res : FPoint;
      s : Float;
      c : Float;
   begin
      s := Sin(Angle);
      c := Cos(Angle);
      Res.X := c * (ToRotate.X - CenterOfR.X) - s * (ToRotate.Y - CenterOfR.Y)
        + CenterOfR.X;
      Res.Y := s * (ToRotate.X - CenterOfR.X) + c * (ToRotate.Y - CenterOfR.Y)
        + CenterOfR.Y;
      return Res;
   end Rotate_About_Of;

   function My_U64 is
     new Ada.Unchecked_Conversion (Time, Unsigned_64);

   function LFSR return Integer is
      Lfsr : Unsigned_16 := Unsigned_16(My_U64(Clock));
      Period : Natural;
      Bit : Unsigned_16;
   begin
      Period := Natural (Lfsr) mod 5 + 1;
      for I in 1..Period loop
         Bit  := (Shift_Left(Lfsr, 0) XOR Shift_Left(Lfsr, 2) XOR
                    Shift_Left(Lfsr, 3) XOR Shift_Left(Lfsr, 5) ) AND 1;
         Lfsr := (Shift_Left(Lfsr, 1) OR Shift_Right(Bit, 15));
      end loop;
      return Integer (Lfsr);
   end LFSR;

end Maths_Tools;
