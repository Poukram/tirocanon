with text; use text;

package Font with SPARK_Mode => Off is

   type Font is array (Character range 'A'..'Z') of Symbole;
   type NumFont is array (Character range '0'..'9') of Symbole;

   NumSymboles : constant NumFont :=
     (((false, true,  true,  true, false),
      (false, true, false,  true, false),
      (false, true, false,  true, false),   -- Zero
      (false, true, false,  true, false),
      (false, true,  true,  true, false)),

      ((false, false, true, false, false),
       (false, true, true, false, false),
       (false, false, true, false, false),  -- Un
       (false, false, true, false, false),
       (false, true, true, true, false)),

      ((false, true, true, true, false),
       (false, false, false, true, false),
       (false, true, true, true, false),    -- Deux
       (false, true, false, false, false),
       (false, true, true, true, false)),

      ((false, true, true, true, false),
       (false, false, false, true, false),
       (false, true, true, true, false),    -- Trois
       (false, false, false, true, false),
         (false, true, true, true, false)),

      ((false, true, false, true, false),
       (false, true, false, true, false),
       (false, true, true, true, false),    -- Quatre
       (false, false, false, true, false),
       (false, false, false, true, false)),

      ((false, true, true, true, false),
       (false, true, false, false, false),
       (false, true, true, true, false),    -- Cinq
       (false, false, false, true, false),
       (false, true, true, true, false)),

      ((false, true, true, true, false),
       (false, true, false, false, false),
       (false, true, true, true, false),    -- Six
       (false, true, false, true, false),
       (false, true, true, true, false)),

      ((false, true, true, true, false),
       (false, true, false, true, false),
       (false, false, false, true, false),  -- Sept
       (false, false, false, true, false),
       (false, false, false, true, false)),

      ((false, true,  true,  true, false),
       (false, true, false,  true, false),
       (false, true, true,  true, false),   -- Huit
       (false, true, false,  true, false),
       (false, true,  true,  true, false)),

      ((false, true, true, true, false),
       (false, true, false, true, false),
       (false, true, true, true, false),    -- Neuf
       (false, false, false, true, false),
       (false, true, true, true, false))
     );

   Symboles : constant Font := (
      ((false, true,  true,  true, false),
       (false, true, false,  true, false),
       (false, true,  true,  true, false),
       (false, true, false,  true, false),
       (false, true, false,  true, false)),

      ((false, true,  true,  true, false),
       (false, true, false,  true, false),
       (false, true,  true,  true,  true),
       (false, true, false, false,  true),
       (false, true,  true,  true,  true)),

      ((false, true,  true,  true, false),
       (false, true, false, false, false),
       (false, true, false, false, false),
       (false, true, false, false, false),
       (false, true,  true,  true, false)),

      ((false, true,  true,  true, false),
       (false, true, false, false,  true),
       (false, true, false, false,  true),
       (false, true, false, false,  true),
       (false, true,  true,  true, false)),

      ((false, true,  true,  true, false),
       (false, true, false, false, false),
       (false, true,  true,  true, false),
       (false, true, false, false, false),
       (false, true,  true,  true, false)),

      ((false, true,  true,  true, false),
       (false, true, false, false, false),
       (false, true,  true,  true, false),
       (false, true, false, false, false),
       (false, true,  false, false, false)),

      ((false, true,  true,  true,  true),
       (false, true, false, false, false),
       (false, true, false,  true,  true),
       (false, true, false, false,  true),
       (false, true,  true,  true,  true)),

      ((false, true, false, true, false),
       (false, true, false, true, false),
       (false, true,  true, true, false),
       (false, true, false, true, false),
       (false, true, false, true, false)),

      ((false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false)),

      ((false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false),
       ( true, false,  true, false, false),
       ( true,  true,  true, false, false)),

      ((false,  true, false, false,  true),
       (false,  true, false,  true, false),
       (false,  true,  true, false, false),
       (false,  true, false,  true, false),
       (false,  true, false, false,  true)),

      ((false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true,  true,  true)),

      (( true, false,  false, false,  true),
       ( true,  true,  false,  true,  true),
       ( true, false,   true, false,  true),
       ( true, false,  false, false,  true),
       ( true, false,  false, false,  true)),

      (( true, false,  false, false,  true),
       ( true,  true,  false, false,  true),
       ( true, false,   true, false,  true),
       ( true, false,  false,  true,  true),
       ( true, false,  false, false,  true)),

      ((false,  true,  true,  true, false),
       (false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false,  true,  true,  true, false)),

      ((false,  true,  true,  true, false),
       (false,  true, false,  true, false),
       (false,  true,  true,  true, false),
       (false,  true, false, false, false),
       (false,  true, false, false, false)),

      ((false,  true,  true,  true, false),
       (false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false,  true,  true,  true, false),
       (false, false, false,  true, false)),

      ((false,  true,  true,  true, false),
       (false,  true, false,  true, false),
       (false,  true,  true,  true, false),
       (false,  true,  true, false, false),
       (false,  true, false,  true, false)),

      ((false,  true,  true,  true, false),
       (false,  true, false, false, false),
       (false,  true,  true,  true, false),
       (false, false, false,  true, false),
       (false,  true,  true,  true, false)),

      ((false,  true,  true,  true, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false),
       (false, false,  true, false, false)),

      ((false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false,  true,  true,  true, false)),

      ((false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false, false,  true, false, false)),

      (( true, false,  true, false,  true),
       ( true, false,  true, false,  true),
       ( true, false,  true, false,  true),
       ( true, false,  true, false,  true),
       (false,  true, false,  true, false)),

      (( true, false, false, false,  true),
       (false,  true, false,  true, false),
       (false, false,  true, false, false),
       (false,  true, false,  true, false),
       ( true, false, false, false,  true)),

      ((false,  true, false,  true, false),
       (false,  true, false,  true, false),
       (false, false,  true, false, false),
       (false, false, false,  true, false),
       (false, false, false, false,  true)),

      (( true,  true, true,  true,  true),
       (false,  true, false, false, false),
       (false, false,  true, false, false),
       (false, false, false,  true, false),
       ( true,  true,  true,  true,  true))
      );

end Font;
