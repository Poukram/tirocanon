with graphics; use graphics;

package body Draw_Game_Assets with SPARK_Mode => Off is
   procedure Draw_Env is
   begin
      Fill_Screen(Sky_Blue);
      Draw_Rect(6, 288, (0,0), Blue);
      Draw_Rect(16, 32, (0, 288), Green);
   end Draw_Env;

   procedure Draw_Canon(Angle : Float) is
   begin
      -- Reinit background .. Begin
      Draw_Rect(25, 32, (16, 288), Sky_Blue);
      -- Reinit background .. Over
      Draw_Circle((20,308), 8, Light_Gray);
      Draw_Rot_Rect(4, 15, (18.0,293.0), (20.0, 308.0), Angle, Gray);
      Draw_Circle((20,308), 1, Black);
      Draw_Rect(16, 32, (0, 288), Green);
   end Draw_Canon;

   procedure Draw_Shell(Prev : Point; Now : Point) is
   begin
      Draw_Circle(Prev, 2, Sky_Blue);
      Draw_Circle(Now, 2, Black);
   end Draw_Shell;

   procedure Draw_Enemy(Prev : Point; Now : Point) is
      TmpPoint : Point;
      ToPrint : constant graphic := Alien;
   begin
      Draw_Rect(14, 14, Prev, Sky_Blue);
      for I in 0..13 loop
         for J in 0..13 loop
            TmpPoint := (I, J) + Now;
            if ToPrint(I,J) then
               Set_Pixel(TmpPoint, Red);
            end if;
         end loop;
      end loop;
   end Draw_Enemy;

   procedure Clear_Shell(P :Point) is
   begin
      Draw_Circle(P, 2, Sky_Blue);
   end Clear_Shell;

   procedure Clear_Infos is
   begin
      Draw_Rect(15, 320, (225, 0), Sky_Blue);
   end Clear_Infos;

   procedure Draw_Rot_Rect(Rwidth : Positive; Rheight : Positive;
                           TopLeftCorner : FPoint; RotPoint : FPoint;
                           Angle : Float; col : Color) is
      TmpFPoint : FPoint;
      TmpPoint : Point;
   begin
      for I in 0..2*(Rwidth-1) loop
         for J in 0..2*(Rheight-1) loop
            TmpFPoint := (Float (I)/2.0, Float (J)/2.0) + TopLeftCorner;
            TmpFPoint := Rotate_About_Of(TmpFPoint, RotPoint, Angle);
            TmpPoint.X := Integer (TmpFPoint.X);
            TmpPoint.Y := Integer (TmpFPoint.Y);
            Set_Pixel(TmpPoint, col);
         end loop;
      end loop;
   end Draw_Rot_Rect;


   procedure Draw_Rect(Rwidth : Positive; Rheight : Positive;
                       TopLeftCorner : Point; col : Color) is
      TmpPoint : Point;
   begin
      for I in 0..Rwidth-1 loop
         for J in 0..Rheight-1 loop
            TmpPoint := (I, J) + TopLeftCorner;
            Set_Pixel(TmpPoint, col);
         end loop;
      end loop;
   end Draw_Rect;

   procedure Draw_Circle(Center : Point; Radius : Positive; col : Color) is
      TmpPoint : Point;
      TmpX : Width;
      TmpY : Height;
   begin
      for Y in -Radius..Radius loop
         for X in -Radius..Radius loop
            if((X*X)+(Y*Y) <= Radius*Radius) then
               TmpX := Center.X + X;
               TmpY := Center.Y + Y;
               TmpPoint := (TmpX, TmpY);
               Set_Pixel(TmpPoint, col);
            end if;
         end loop;
      end loop;
   end Draw_Circle;

end Draw_Game_Assets;
