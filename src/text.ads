with screen_interface; use screen_interface;

package Text with SPARK_Mode => Off is

   type Symbole is array (Positive range 1 .. 5, Positive range 1 .. 5) of Boolean;

   procedure Print_Symbole(Letter : Character; Location : Point; Width : Positive)
     with
       Pre => Width < 6;
   procedure Print_Number(Number : Character; Location : Point; Width : Positive)
     with
       Pre => Width < 6;
   procedure Print_Line(Line : String; Location : Point; Width : Positive)
     with
       Pre => Width < 6;
   procedure Print_NumLine(Line : String; Location : Point; Width : Positive)
     with
       Pre => Width < 6;

end Text;
