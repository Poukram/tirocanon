with screen_interface; use screen_Interface;
with Game_State; use Game_State;
with Ada.Real_Time; use Ada.Real_Time;

procedure game_main
is
   G : State;
   State : Touch_State;
begin
   Screen_Interface.Initialize;
   Initialize_Game(G);

   loop
      State := Get_Touch_State;
      if State.Touch_Detected then
         Fire_Shell(G, (State.X, State.Y));
      end if;
      Update(G);
      delay until Clock + Milliseconds (5);
   end loop;


end game_main;
