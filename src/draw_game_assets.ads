with screen_interface; use screen_interface;
with Maths_Tools; use Maths_Tools;

package Draw_Game_Assets with SPARK_Mode => Off is
   procedure Draw_Env;
   procedure Draw_Canon(Angle : Float);
   procedure Draw_Shell(Prev : Point; Now : Point);
   procedure Clear_Shell(P : Point);
   procedure Clear_Infos;
   procedure Draw_Enemy(Prev : Point; Now : Point);
   procedure Draw_Rect(Rwidth : Positive; Rheight : Positive;
                       TopLeftCorner : Point; col : Color)
     with
       Pre => TopLeftCorner.X + Rwidth <= 240 and
       TopLeftCorner.Y + Rheight <= 320;
   procedure Draw_Rot_Rect(Rwidth : Positive; Rheight : Positive;
                           TopLeftCorner : FPoint; RotPoint : FPoint;
                           Angle : Float; col : Color)
     with
       Pre => TopLeftCorner.X >= 0.0 and TopLeftCorner.X <= 240.0 and
     TopLeftCorner.Y >= 0.0 and TopLeftCorner.Y <= 320.0 and
     RotPoint.X >= 0.0 and RotPoint.X <= 240.0 and
     RotPoint.Y >= 0.0 and RotPoint.Y <= 320.0;
   procedure Draw_Circle(Center : Point; Radius : Positive; col : Color)
     with
       Pre => Center.X + Radius <= 240 and Center.X - Radius >= 0 and
     Center.Y + Radius <+ 320 and Center.Y - Radius >= 0;
end Draw_Game_Assets;
