#! /usr/bin/zsh
PATH=/usr/gnat/bin:$PATH

echo Flashing game on STM32 ... BEGIN

# Building elf
./build.sh
# Converting elf to ihex
arm-eabi-objcopy -O ihex game_main game_main.hex
# Flashing board with ihex file
st-flash --format ihex write game_main.hex

echo Flashing game on STM32 ... DONE
