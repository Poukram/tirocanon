#! /usr/bin/zsh
PATH=/usr/gnat/bin:$PATH

echo Launching game on STM32
st-util > ./session.log 2>&1 &;

arm-eabi-gdb -ex "target remote :4242" -ex "load" game_main
